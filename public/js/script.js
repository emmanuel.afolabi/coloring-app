function loadContent(path) {
    var container = document.getElementById('main');
    fetch('/' + path)
        .then(function (response) {
            return response.text();
        }).then(function (text) {
//            console.log(text);
            container.innerHTML = text;
        });
}

function hideBox() {
    $('.message-box-container').addClass('hidden');
}

function showBox(paint) {
    $('.message-box-container').removeClass('hidden');
    $('#message-box input.delete').attr('onclick', 'deletePaint("' + paint + '")');
}

$('.load-btn').click(function () {
    $('li.active').removeClass('active');
    $(this).addClass('active');
});

function deletePaint(paintid) {
    console.log('del!');
    fetch('/delete/' + paintid).then(function (res) {
        return res.text();
    }).then(function (text) {
        if (text == 'deleted') {
            hideBox();
            loadContent('loadWorks');
        }
    });
}

function getColor(paintid) {
    if (paintid != '') {
        fetch('/colordata/' + paintid).then(function (res) {
            return res.text();
        }).then(function (colordata) {
            console.log(JSON.parse(colordata));
            var colors = JSON.parse(colordata);
            var paintname = document.getElementById('paintname');
            if (paintname != null)
                paintname.value = colors.paintname;

            var mysvg = document.getElementById(paintid).contentDocument.getElementsByTagName('svg')[0];
            // var mysvg = "Life";

            var paths = [];
            paths.push(mysvg.getElementsByTagName('path'), mysvg.getElementsByTagName('circle'), mysvg.getElementsByTagName('ellipse'), mysvg.getElementsByTagName('polygon'), mysvg.getElementsByTagName('rect'));

            var index = 0;
            for (var i = 0; i < paths.length; i++) {
                for (var j = 0; j < paths[i].length; j++) {
                    paths[i][j].style.fill = colors['piece-' + index];
                    index++;
                }
            }
        });
    }
}

$(function() {

  var loc = window.location.href; // returns the full URL
  if(/#mix/.test(loc)) {
    $('body').addClass('color-bg');
  }
  if(/#works/.test(loc)) {
    $('body').addClass('color-bg header-relate');
  }

});

function doCapture() {


    //get svg element.
    var svg = document.getElementById("Layer_1");

    //get svg source.
    var serializer = new XMLSerializer();
    var source = serializer.serializeToString(svg);

    //add name spaces.
    if(!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)){
        source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
    }
    if(!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)){
        source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
    }

    //add xml declaration
    source = '<?xml version="1.0" standalone="no"?>\r\n' + source;

    //convert svg source to URI data scheme.
    var url = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(source);

    //set url value to a element's href attribute.
    document.getElementById("link").href = url;
    //you can download svg file by right click menu.





	// // Convert the div to image (canvas)
	// html2canvas(document.getElementById("downloadableColoring")).then(function (canvas) {

	// 	// Get the image data as JPEG and 0.9 quality (0.0 - 1.0)
	// 	console.log(canvas.toDataURL("image/jpeg", 0.9));
	// });



    // // the canvg call that takes the svg xml and converts it to a canvas
    // canvg('canvas', $("#Layer_1")[0].innerHTML);

    // // the canvas calls to output a png
    // var canvas = document.getElementById("canvas");
    // var img = canvas.toDataURL("image/png");
    // // do what you want with the base64, write to screen, post to server, etc...
}